CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_ODEME_LIST"    as object
    (
      ode_sira_no            number,
      isyeri_ode_tip_kod     varchar2(32),
      ode_tip_kod            varchar2(32),
      prov_no                varchar2(255),
      prov_tarih             varchar2(255),
      Chippin_trx_no         varchar2(32),
      Chippin_Id             number,
      tutar                  number
    )
/

