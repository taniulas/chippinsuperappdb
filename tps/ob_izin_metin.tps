CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_IZIN_METIN"    as object
    (
     izin_tip            Number, --1:kvk , 2:etk , 3:ortak
     Form_Izin_id        Varchar2(255),
     Form_Versiyon_no    Number,
     Form_Versiyon_id    number,
     Metin_kisa          varchar2(4000) ,
     Metin_Uzun          clob
    )
/

