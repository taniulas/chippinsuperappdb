CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_URUN_LIST"    as object
    (
     sira_no   number,
     urunkod   varchar2(250),
     adet      number,
     birim_kod varchar2(250) ,
     birim_fiyat  number,
     tutar        number,
     ind_oran     number,
     ind_tutar    number,
     kdv_oran     number,
     reyon        varchar2(500),
     firma_indirim_tutar number
    )
/

