CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_NUMBER2"                                          as object
    (
      v_number1 number(24),
      v_number2 number(24)
    )
/

