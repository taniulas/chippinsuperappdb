CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_ARAC_BILGI"    as object
    (
         Plaka varchar2(100),
         Marka varchar2(500),
         Model varchar2(500),
         Yil   number
    )
/

