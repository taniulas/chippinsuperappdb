CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_IZIN_LIST"    as object
    (
     izinTip        number, --1:kvk , 2:etk , 3:ortak
     FormId         varchar2(255),
     VersiyonId     number,
     versiyonNo     varchar2(255)
    );
/

