CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_KAMPANYA_LIST"    as object
    (
      kampanya_tip              varchar2(10), --1:Indirim 2: Puan 3:Chippin �deme Indirimi
      kaynak_kamp_kod           varchar2(30),
      urun_sira_no              number,
      kazanilan_fayda_tutar     number,
      harcanan_fayda_tutar      number,
      kullanildi                number(1)
    )
/

