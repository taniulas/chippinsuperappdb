CREATE OR REPLACE TYPE CHIPPINSUPERAPP."OB_ILETISIM_TERCIH"    as object
    (
      iletisimAdresTipi number(2), --1:CepTelSms 2:CepTelArama 3:Email
      iletisimTercih    number(1)  --1:izinli 0:Yasakli
    )
/

