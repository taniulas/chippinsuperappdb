CREATE OR REPLACE Function CHIPPINSUPERAPP.KARAKTERDUZENLE(p_str in varchar2)
return varchar2 is
Begin
       return TRIM(REGEXP_REPLACE(p_str, '[^0-9A-Za-z]', ''));
End;
/

