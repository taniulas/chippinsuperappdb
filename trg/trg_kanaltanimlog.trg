CREATE OR REPLACE TRIGGER CHIPPINSUPERAPP.TRG_KANALTANIMLOG
  AFTER DELETE OR INSERT OR UPDATE ON KANALTANIM
  FOR EACH ROW
DECLARE
  lch_audsid  VARCHAR2(200);
  lch_machine VARCHAR2(200);
  lch_tipp    VARCHAR2(200);
  log_id_seq  NUMBER;
  lch_owner   VARCHAR2(30);
  lch_name    VARCHAR2(30);
  lnm_line    PLS_INTEGER;
  lch_type    VARCHAR2(30);
  lch_sqlerrm VARCHAR2(4000);
BEGIN
  /*Dynamic Created !*/
  If NOT TRGUTIL.ISDISABLETRG Then
  
    OWA_UTIL.who_called_me(lch_owner, lch_name, lnm_line, lch_type);
  
    BEGIN
      SELECT USERENV('SESSIONID') INTO lch_audsid FROM DUAL;
    
      SELECT SUBSTR(machine, 1, 100)
        INTO lch_machine
        FROM v$session
       WHERE audsid = lch_audsid;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
    BEGIN
      SELECT SYS_CONTEXT('userenv', 'ip_address') INTO lch_tipp FROM DUAL;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        lch_tipp := '';
      WHEN OTHERS THEN
        lch_tipp := '';
    END;
  
    SELECT SEQ_KANALTANIMLOG_ID.NEXTVAL INTO log_id_seq FROM DUAL;
  
    IF INSERTING THEN
      INSERT INTO KANALTANIMLOG
        (log_id,
         KANAL_KOD,
         KANAL_GRUP_KOD,
         KANAL_ALT_KOD,
         KANAL_ACIKLAMA,
         IYS_KAYNAK,
         
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected,
         RAPORLAMAYA_DAHIL_MI)
      VALUES
        (log_id_seq,
         :NEW.KANAL_KOD,
         :NEW.KANAL_GRUP_KOD,
         :NEW.KANAL_ALT_KOD,
         :NEW.KANAL_ACIKLAMA,
         :NEW.IYS_KAYNAK,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'INSERT',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack,
         :NEW.RAPORLAMAYA_DAHIL_MI);
    END IF;
  
    IF UPDATING THEN
      INSERT INTO KANALTANIMLOG
        (log_id,
         KANAL_KOD,
         KANAL_GRUP_KOD,
         KANAL_ALT_KOD,
         KANAL_ACIKLAMA,
         IYS_KAYNAK,
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected,
         RAPORLAMAYA_DAHIL_MI
         )
      VALUES
        (log_id_seq,
         :NEW.KANAL_KOD,
         :NEW.KANAL_GRUP_KOD,
         :NEW.KANAL_ALT_KOD,
         :NEW.KANAL_ACIKLAMA,
         :NEW.IYS_KAYNAK,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'UPDATE_NEW',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack,
         :NEW.RAPORLAMAYA_DAHIL_MI);
    
      INSERT INTO KANALTANIMLOG
        (log_id,
         KANAL_KOD,
         KANAL_GRUP_KOD,
         KANAL_ALT_KOD,
         KANAL_ACIKLAMA,
         IYS_KAYNAK,
         
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected,
         RAPORLAMAYA_DAHIL_MI)
      VALUES
        (log_id_seq,
         :OLD.KANAL_KOD,
         :OLD.KANAL_GRUP_KOD,
         :OLD.KANAL_ALT_KOD,
         :OLD.KANAL_ACIKLAMA,
         :OLD.IYS_KAYNAK,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'UPDATE_OLD',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack,
         :OLD.RAPORLAMAYA_DAHIL_MI);
    END IF;
  
    IF DELETING THEN
      INSERT INTO KANALTANIMLOG
        (log_id,
         KANAL_KOD,
         KANAL_GRUP_KOD,
         KANAL_ALT_KOD,
         KANAL_ACIKLAMA,
         IYS_KAYNAK,
         
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected,
         RAPORLAMAYA_DAHIL_MI)
      VALUES
        (log_id_seq,
         :OLD.KANAL_KOD,
         :OLD.KANAL_GRUP_KOD,
         :OLD.KANAL_ALT_KOD,
         :OLD.KANAL_ACIKLAMA,
         :OLD.IYS_KAYNAK,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'DELETE',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack,
         :OLD.RAPORLAMAYA_DAHIL_MI);
    END IF;
  End If; --trgutil
EXCEPTION
  WHEN OTHERS THEN
    lch_sqlerrm := SQLERRM;
  
    INSERT INTO KANALTANIMLOG
      (log_id,
       terminal,
       terminal_ip,
       log_user,
       log_date,
       log_action,
       log_affected,
       log_sqlerrm)
    VALUES
      (log_id_seq,
       lch_machine,
       lch_tipp,
       Nvl('',
           CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                  CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
       SYSDATE,
       'EXCEPTION',
       lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type,
       lch_sqlerrm || Chr(10) ||
       ' ***************************** - format_call_stack - ***************************** ' ||
       CHR(10) || CHR(10) || DBMS_UTILITY.format_call_stack);
END;
/

