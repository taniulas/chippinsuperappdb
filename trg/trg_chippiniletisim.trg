CREATE OR REPLACE TRIGGER CHIPPINSUPERAPP.TRG_CHIPPINILETISIM
  AFTER DELETE OR UPDATE OR INSERT on CHIPPINILETISIM
  For Each Row
DECLARE
v_row_islemcoklukontrol islemcoklukontrol%rowtype;
BEGIN


   Begin
      Select * into v_row_islemcoklukontrol from  islemcoklukontrol where  id = 2 and id_sirano = 1 ;
   Exception
     When Others Then
      v_row_islemcoklukontrol:=null;

   End;


  if inserting Then
    if  :new.ilk_izin_tarihi is not null  or :new.ilk_yasaklilik_tarihi is not null then

           insert into iysgonderim(islem_id,
                                   iys_marka_kod,
                                   iletisim_iys_tipi,
                                   uye_isyeri_kod_izinli_yasakli,
                                   iys_kurum_kod,
                                   veri_tip,
                                   tel_email,
                                   iys_kaynak,
                                   onay_red,
                                   iletisim_tarih,
                                   son_guncellenme_tarihi,
                                   iys_alici_tip)
                            values(:new.islem_id,
                                   :new.iys_marka_kodu,
                                   :new.iletisim_adres_tip,
                                   nvl(:new.ilk_izin_isyeri_kod,:new.ilk_yasakli_isyeri_kod),
                                   v_row_islemcoklukontrol.param2,
                                   decode(:new.iletisim_adres_tip,1,1,2,1,3,2),
                                   :new.iletisim_tip_deger,
                                   :new.kanal_kod,
                                   case when :new.ilk_izin_tarihi is not null then
                                        1
                                   when :new.ilk_yasaklilik_tarihi is not null then
                                        2
                                   end,
                                   sysdate,
                                   sysdate,
                                   1);


  end if;
 elsif updating then
       if  (:old.ilk_izin_tarihi is  null  and :new.ilk_izin_tarihi is not null )
         or (:old.ilk_yasaklilik_tarihi is null and :new.ilk_yasaklilik_tarihi is not null) then  
 
           insert into iysgonderim(islem_id,
                                   iys_marka_kod,
                                   iletisim_iys_tipi,
                                   uye_isyeri_kod_izinli_yasakli,
                                   iys_kurum_kod,
                                   veri_tip,
                                   tel_email,
                                   iys_kaynak,
                                   onay_red,
                                   iletisim_tarih,
                                   son_guncellenme_tarihi,
                                   iys_alici_tip)
                            values(:new.islem_id,
                                   :new.iys_marka_kodu,
                                   :new.iletisim_adres_tip,
                                   nvl(:new.ilk_izin_isyeri_kod,:new.ilk_yasakli_isyeri_kod),
                                   v_row_islemcoklukontrol.param2,
                                   decode(:new.iletisim_adres_tip,1,1,2,1,3,2),
                                   :new.iletisim_tip_deger,
                                   :new.kanal_kod,
                                   case when :new.ilk_izin_tarihi is not null then
                                        1
                                   when :new.ilk_yasaklilik_tarihi is not null then
                                        2
                                   end,
                                   sysdate,
                                   sysdate,
                                   1);
 
 
     end if;
 end if;

EXCEPTION
  WHEN OTHERS THEN
    NULL;

END TRG_CHIPPINILETISIM;
/

