CREATE OR REPLACE TRIGGER CHIPPINSUPERAPP.TRG_ISYERILOG
  AFTER DELETE OR INSERT OR UPDATE ON ISYERI
  FOR EACH ROW
DECLARE
  lch_audsid  VARCHAR2(200);
  lch_machine VARCHAR2(200);
  lch_tipp    VARCHAR2(200);
  log_id_seq  NUMBER;
  lch_owner   VARCHAR2(30);
  lch_name    VARCHAR2(30);
  lnm_line    PLS_INTEGER;
  lch_type    VARCHAR2(30);
  lch_sqlerrm VARCHAR2(4000);
BEGIN
  /*Dynamic Created !*/
  If NOT TRGUTIL.ISDISABLETRG Then
  
    OWA_UTIL.who_called_me(lch_owner, lch_name, lnm_line, lch_type);
  
    BEGIN
      SELECT USERENV('SESSIONID') INTO lch_audsid FROM DUAL;
    
      SELECT SUBSTR(machine, 1, 100)
        INTO lch_machine
        FROM v$session
       WHERE audsid = lch_audsid;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
  
    BEGIN
      SELECT SYS_CONTEXT('userenv', 'ip_address') INTO lch_tipp FROM DUAL;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        lch_tipp := '';
      WHEN OTHERS THEN
        lch_tipp := '';
    END;
  
    SELECT SEQ_ISYERILOG_ID.NEXTVAL INTO log_id_seq FROM DUAL;
  
    IF INSERTING THEN
      INSERT INTO ISYERILOG
        (log_id,
         FIRM_ID,
         ISYERI_KOD,
         SEKTOR_KOD,
         ISLEM_KAYIT_TARIHI,
         
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected)
      VALUES
        (log_id_seq,
         :NEW.FIRM_ID,
         :NEW.ISYERI_KOD,
         :NEW.SEKTOR_KOD,
         :NEW.ISLEM_KAYIT_TARIHI,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'INSERT',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack);
    END IF;
  
    IF UPDATING THEN
      INSERT INTO ISYERILOG
        (log_id,
         FIRM_ID,
         ISYERI_KOD,
         SEKTOR_KOD,
         ISLEM_KAYIT_TARIHI,
         
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected)
      VALUES
        (log_id_seq,
         :NEW.FIRM_ID,
         :NEW.ISYERI_KOD,
         :NEW.SEKTOR_KOD,
         :NEW.ISLEM_KAYIT_TARIHI,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'UPDATE_NEW',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack);
    
      INSERT INTO ISYERILOG
        (log_id,
         FIRM_ID,
         ISYERI_KOD,
         SEKTOR_KOD,
         ISLEM_KAYIT_TARIHI,
         
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected)
      VALUES
        (log_id_seq,
         :OLD.FIRM_ID,
         :OLD.ISYERI_KOD,
         :OLD.SEKTOR_KOD,
         :OLD.ISLEM_KAYIT_TARIHI,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'UPDATE_OLD',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack);
    END IF;
  
    IF DELETING THEN
      INSERT INTO ISYERILOG
        (log_id,
         FIRM_ID,
         ISYERI_KOD,
         SEKTOR_KOD,
         ISLEM_KAYIT_TARIHI,
         
         terminal,
         terminal_ip,
         log_user,
         log_date,
         log_action,
         log_affected)
      VALUES
        (log_id_seq,
         :OLD.FIRM_ID,
         :OLD.ISYERI_KOD,
         :OLD.SEKTOR_KOD,
         :OLD.ISLEM_KAYIT_TARIHI,
         
         lch_machine,
         lch_tipp,
         Nvl('',
             CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                    CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
         Sysdate,
         'DELETE',
         lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type ||
         Chr(10) ||
         ' ***************************** - format_call_stack - ***************************** ' ||
         Chr(10) || Chr(10) || dbms_utility.format_call_stack);
    END IF;
  End If; --trgutil
EXCEPTION
  WHEN OTHERS THEN
    lch_sqlerrm := SQLERRM;
  
    INSERT INTO ISYERILOG
      (log_id,
       terminal,
       terminal_ip,
       log_user,
       log_date,
       log_action,
       log_affected,
       log_sqlerrm)
    VALUES
      (log_id_seq,
       lch_machine,
       lch_tipp,
       Nvl('',
           CONCAT(SYS_CONTEXT('USERENV', 'SESSION_USER'),
                  CONCAT(' - ', SYS_CONTEXT('USERENV', 'OS_USER')))),
       SYSDATE,
       'EXCEPTION',
       lch_owner || '.' || lch_name || '.' || lnm_line || '.' || lch_type,
       lch_sqlerrm || Chr(10) ||
       ' ***************************** - format_call_stack - ***************************** ' ||
       CHR(10) || CHR(10) || DBMS_UTILITY.format_call_stack);
END;
/

