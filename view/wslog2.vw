create or replace force view chippinsuperapp.wslog2 as
select
ws_seq,
ws_kod,
isyeri_kod,
yetkili_kod,
durum,
tarih,
dis_tarih,
pod_no,
tarih_timestamp,
case when nvl(dbms_lob.getlength(str_clob),0) <= 4000 then to_clob(str) else str_clob end as strclob,
kvk_izin_form_id,
kvk_versiyon_id,
kvk_versiyon_no,
kvk_kisa_metin,
kvk_uzun_metin,
etk_izin_form_id,
etk_versiyon_id,
etk_versiyon_no,
etk_kisa_metin,
etk_uzun_metin,
ortak_izin_form_id,
ortak_versiyon_id,
ortak_versiyon_no,
ortak_kisa_metin,
ortak_uzun_metin
from wslog w;

